
const  MeetingStatus  = {
  Loading :0,
  Succeeded : 1,
  Failed : 2
}

export default MeetingStatus;

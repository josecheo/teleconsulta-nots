import { useState } from 'react';
import axios from 'axios';
import FileSaver from 'file-saver';

const useDonwload = ({
  initialUri = '',
  type = 'application/pdf',
  loading = false,
}) => {
  const [isLoading, setLoading] = useState(loading);

  const download = (params, { uri, fileName, extension } = { uri: null, fileName: 'donwload', extension: 'pdf' }) => {
    setLoading(true);
    axios.get(initialUri || uri, {
      params,
      responseType: 'blob',
    }).then((response) => {
      FileSaver.saveAs(new Blob([response.data], { type }), `${fileName}.${extension}`);
      setLoading(false);
    })
      .catch((e) => {
        setLoading(false);
      });
  };

  return {
    isLoading,
    download,
  };
};

export default useDonwload;

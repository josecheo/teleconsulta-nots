import axios from 'axios';

const $axios = axios.create({
  baseURL: '/api',
});

// Interceptores
$axios.interceptors.request.use(
  // Success
  (config) => {
    const c = { ...config };
    console.log(c);
    // c.headers.Authorization = `Bearer ${token}`;
    return c;
  },

  // Error
  (error) => Promise.reject(error),
);

$axios.interceptors.response.use(
  // Success
  (response) => response,

  // Error
  (error) => Promise.reject(error),
);

export default $axios;

import axios, { CancelToken } from "axios";
import { useEffect, useState, useContext } from "react";
import { DispatchMessageContext } from "../providers/message/provider";
import handleRequest from "../utils/handle-request";

async function submitForm(
  uri,
  form,
  cancelToken,
  contentType = "application/json",
  method = "post"
) {
  let formData;
  if (contentType === "multipart/form-data") {
    formData = new FormData();
    Object.keys(form).forEach((property) => {
      if (property === "files") {
        form.files.forEach((file) => {
          formData.append("files", file);
        });
      } else {
        formData.set(property, form[property]);
      }
    });
  } else {
    formData = form;
  }
  const config = { cancelToken };
  return handleRequest(method, uri, formData, config);
}

function useSubmitForm({
  initialUri = "",
  initialMethod = "post",
  initialForm = {},
  contentType = "application/json",
}) {
  const dispatch = useContext(DispatchMessageContext);
  const [uri, setUri] = useState(initialUri);
  const [form, setForm] = useState(initialForm);
  const [method, setMethod] = useState(initialMethod);
  const [isLoading, setIsLoading] = useState(false);
  const { CancelToken } = axios;
  const { token, cancel } = CancelToken.source();

  useEffect(() => {
    dispatch({ type: "reset" });
    return () => cancel("component unmount");
  }, [uri, form]);

  const submit = async (data = null) => {
    dispatch({ type: "reset" });
    // eslint-disable-next-line consistent-return
    const fetch = async () => {
      setIsLoading(true);
      const response = await submitForm(
        uri,
        data || form,
        token,
        contentType,
        method
      );
      setIsLoading(false);
      if (response) {
        dispatch({ type: "success", message: response.message });
        return response;
      }
      dispatch({ type: "error", message: response.message });
      return response;
    };

    return fetch();
  };

  return {
    uri,
    setUri,
    form,
    setForm,
    submit,
    isLoading,
    setMethod,
  };
}

export default useSubmitForm;

import {
  useState, useEffect,
} from 'react';
import axios from 'axios';
import moment from 'moment';
import handleRequest from '../utils/handle-request';
import { setCookie } from './useAuthentication';

async function doRequest(method, uri, form, cancelToken, contentType = 'application/json') {
  let formData;
  if (contentType === 'multipart/form-data') {
    formData = new FormData();
    Object.keys(form).forEach((property) => {
      // Formateo de fecha
      if (moment.isMoment(form[property])) {
        const date = form[property].format('YYYY-MM-DD');
        formData.set(property, date);
      } else {
        formData.set(property, form[property]);
      }
    });
  } else {
    formData = form;
  }
  const config = { cancelToken };
  return handleRequest(method, uri, formData, config);
}

function useFetch({
  initialUri = '',
  initialForm = {},
  contentType = 'application/json',
  initalMethod = 'get',
  loading = false,
}) {
  const [uri, setUri] = useState(initialUri);
  const [form, setForm] = useState(initialForm);
  const [method, setMethod] = useState(initalMethod);
  const [isLoading, setIsLoading] = useState(loading);
  const { CancelToken } = axios;
  const { token, cancel } = CancelToken.source();

  useEffect(() => () => cancel('component unmount'), [uri, form]);


  const fetch = async (data = null, config = {}) => {
    setIsLoading(true);
    const response = await doRequest(
      config.method || method,
      config.uri || uri,
      data || form,
      token,
      config.contentType || contentType,
    );
    setIsLoading(false);

    if (response && response.status === 403 && typeof document !== 'undefined') {
      setCookie('RUC', null, -1);
      setCookie('ROLE', null, -1);
      setCookie('sessionId', null, -1);
      document.location.href = '/auth/login';
    }

    return response;
  };

  return {
    uri, setUri, form, setForm, fetch, isLoading, setMethod,
  };
}

export default useFetch;

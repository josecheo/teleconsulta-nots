import { useState } from 'react';
import useSubmitForm from './useSubmitForm';
import useFetch from './useFetch';

const configFormFetch = {
  method: 'post',
  initialUri: '/api/v1/assignment/list_product_user_assignment',
};
const configGetFetch = {
  method: 'get',
  initialUri: '/api/v1/assignment/list_product_user_assignment',
};

export const useManageUser = () => {
  const [loading, setLoading] = useState(false);
  const getFetch = useFetch(configGetFetch);
  const formFetch = useSubmitForm(configFormFetch);

  async function getUser(id) {
    setLoading(true);
    const response = await getFetch.fetch({ id });
    setLoading(false);
    return response;
  }

  async function sendForm(data) {
    setLoading(true);
    const response = await formFetch.submit(data);
    setLoading(false);
    return response;
  }

  return {
    loading,
    getUser,
    sendForm,
  };
};


export default {
  useManageUser,
};

import { useState, useCallback, useEffect } from 'react';
import axios from 'axios';
import handleRequest from '../utils/handle-request';

async function fetchList(uri, params, currentPage, resultsPerPage, cancelToken, method) {
  // const data = { ...params };
  const data = { ...params, page: currentPage, number_records: resultsPerPage };
  const config = { cancelToken };
  return handleRequest(method, uri, data, config);
}

function getList(uri, parameters, method = 'post') {
  const [data, setData] = useState([]);
  const [from, setFrom] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [perPage, setPerPage] = useState(10);
  const [totalItems, setTotalItems] = useState(0);
  const [params, setParams] = useState(parameters); // Params tiene el estado actual

  const reload = useCallback(() => {
    const { CancelToken } = axios;
    const { token, cancel } = CancelToken.source();
    const fetch = async () => {
      setIsLoading(true);
      const response = await fetchList(uri, params, currentPage, perPage, token, method);
      if (response.success) {
        setData(response.data.data);
        setTotalItems(response.data.total);
        setIsLoading(false);
      }
    };

    fetch();

    return () => cancel('component unmount');
  }, [currentPage, perPage, params]);

  useEffect(() => {
    setIsLoading(false);

    const { CancelToken } = axios;
    const { token, cancel } = CancelToken.source();
    const fetch = async () => {
      setIsLoading(true);
      const response = await fetchList(uri, params, currentPage, perPage, token, method);
      // const response = await fetchList(uri, {id_user_responsable: 6 }
      // , currentPage, perPage, token);
      if (response.success) {
        setFrom(response.data.from);
        setData(response.data.data);
        setTotalItems(response.data.total);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      }
    };

    fetch();

    return () => cancel('component unmount');
  }, [currentPage, perPage, params]);

  const changePage = (page) => {
    setCurrentPage(page);
  };

  const changeParams = (parms) => {
    setCurrentPage(1);
    setParams(parms);
  };

  return {
    data,
    currentPage,
    totalItems,
    perPage,
    isLoading,
    changePage,
    setPerPage,
    reload,
    from,
    params,
    changeParams,
  };
}

export default getList;

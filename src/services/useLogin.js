import {
  useContext, useState, useCallback, useEffect,
} from 'react';
import axios from 'axios';
import { AuthDispatchContext } from '@providers/auth/provider';
import handleRequest from '../utils/handle-request';
import { setCookie } from './useAuthentication';
import { DispatchMessageContext } from '../providers/message/provider';

async function login({ username, password, tokenCaptcha }, cancelToken) {
  const method = 'post';
  const uri = '/login';
  const data = {
    username,
    password,
    tokenCaptcha,
  };

  const config = {
    cancelToken,
  };
  return handleRequest(method, uri, data, config);
}

async function logout(cancelToken) {
  const method = 'post';
  const uri = '/logout';
  const data = {};
  const config = {
    cancelToken,
  };
  return handleRequest(method, uri, data, config);
}

function useLogin() {
  const dispatch = useContext(AuthDispatchContext);
  const dispatchMessage = useContext(DispatchMessageContext);
  const [form, setForm] = useState({
    username: '',
    password: '',
    tokenCaptcha: '',
  });
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    dispatchMessage({ type: 'reset' });
  }, [form]);

  const doLogin = useCallback(() => {
    const { CancelToken } = axios;
    const { token, cancel } = CancelToken.source();
    dispatchMessage({ type: 'reset' });
    // eslint-disable-next-line consistent-return
    const fetch = async () => {
      setIsLoading(true);
      const res = await login(form, token);
      if (res.success) {
        if (res.data.access) {
          setIsLoading(false);
          setCookie('sessionId', res.data.token);
          setCookie('ROLE', res.data.roles[0].authority);
          return dispatch({ type: 'LOGIN', user: res.data });
        }
        return dispatch({ type: 'RESET_PASSWORD', token: res.data.token });
      }
      if (!res.success) {
        setIsLoading(false);
        return dispatchMessage({ type: 'error', message: res });
      }
    };

    fetch();

    return () => cancel('component unmount');
  }, [form]);

  return [setForm, doLogin, isLoading];
}

function useLogout() {
  // const dispatch = useContext(AuthDispatchContext);
  // const dispatchMessage = useContext(DispatchMessageContext);
  const [isLoading, setIsLoading] = useState(false);
  const doLogout = useCallback(() => {
    const { CancelToken } = axios;
    const { token, cancel } = CancelToken.source();

    const fetch = async () => {
      setIsLoading(true);
      const res = await logout(token);
      setIsLoading(false);
      if (!res.success) {
        // return dispatchMessage({ type: 'error', message: res.message });
      }
      // dispatchMessage({ type: 'reset' });
      // return dispatch({ type: 'LOGOUT' });
    };

    fetch();

    return () => cancel('component unmount');
  });

  return [doLogout, isLoading];
}

export { useLogin, useLogout };

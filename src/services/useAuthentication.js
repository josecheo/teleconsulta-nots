import { useState, useEffect } from 'react';

export function getCookie(name) {
  if (typeof document !== 'undefined') {
    const decodedCookie = decodeURIComponent(document.cookie);
    const cookies = decodedCookie.split(';');
    const exists = cookies.find((el) => el.trim().indexOf(name) === 0);
    if (exists) {
      return exists.trim().substring(name.length + 1);
    }
  }
  return '';
}

export function setCookie(cname, cvalue, exdays = 1) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  const expires = `expires=${d.toUTCString()}`;
  document.cookie = `${cname}=${cvalue};${expires};path=/`;
}

export function getSessionStorage(name) {
  return typeof sessionStorage === 'undefined' ? null : sessionStorage.getItem(name) || null;
}

export function setSessionStorage(name, value) {
  if (typeof sessionStorage !== 'undefined') {
    sessionStorage.setItem(name, value);
  }
}

export function getLocalStorage(name) {
  return typeof localStorage !== 'undefined' ? localStorage.getItem(name) : null;
}

export function setLocalStorage(name, value) {
  if (typeof localStorage !== 'undefined') {
    localStorage.setItem(name, value);
  }
}

function getIsLoggedIn() {
  const loggedIn = getCookie('isLoggedIn');
  return (loggedIn === 'true') || false;
}

function useAuthentication() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const loggedIn = getIsLoggedIn();
    setIsLoggedIn(loggedIn);
    return (() => { });
  });

  return [isLoggedIn];
}

const isLoggedIn = getIsLoggedIn();

export { useAuthentication, isLoggedIn };

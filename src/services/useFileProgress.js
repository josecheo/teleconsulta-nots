import axios from 'axios';
import {
  useState,
} from 'react';
import handleResponse from '@utils/handle-response';
import { handleError } from '../utils/handle-request';

const useSendFile = () => {
  const [progress, setProgress] = useState(0);

  const sendFile = async (url, form) => {
    try {
      const requestConfig = {
        method: 'post',
        url,
        data: null,
        config: { headers: { 'Content-Type': 'multipart/form-data' } },
        onUploadProgress: (progressEvent) => {
          const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
          if (totalLength !== null) {
            setProgress(Math.round((progressEvent.loaded * 100) / totalLength));
          }
        },
      };

      const formData = new FormData();
      Object.keys(form).forEach((property) => {
        formData.set(property, form[property]);
      });

      requestConfig.data = formData;

      const response = await axios(requestConfig);
      const payload = await handleResponse(response);
      return payload;
    } catch (error) {
      return handleError(error);
    }
  };
  return {
    progress,
    sendFile,
    setProgress,
  };
};

export default useSendFile;

import axios from 'axios';
import {
  AudioVideoFacade,
  ConsoleLogger,
  DefaultDeviceController,
  DefaultDOMWebSocketFactory,
  DefaultMeetingSession,
  DefaultModality,
  DefaultPromisedWebSocketFactory,
  DeviceChangeObserver,
  FullJitterBackoff,
  LogLevel,
  MeetingSession,
  MeetingSessionConfiguration,
  ReconnectingPromisedWebSocket
} from 'amazon-chime-sdk-js';
import FullDeviceInfoType from '../types/FullDeviceInfoType';
import DeviceType from '../types/DeviceType';
import RosterType from '../types/RosterType';
import throttle from 'lodash/throttle';
import VideoRemote from '../components/VideoRemote/index';

class ChimeSdkWrapper  {

   WEB_SOCKET_TIMEOUT_MS = 10000;
   ROSTER_THROTTLE_MS = 400;

  meetingSession = null;

  audioVideo = null;


  title = null;

  name = null;

  region = null;

  currentAudioInputDevice= null;
  currentAudioOutputDevice=  null;
  currentVideoInputDevice=  null;
  configuration = null;
  audioInputDevices = [];
  audioOutputDevices = [];
  videoInputDevices = [];
  roster = {};
  rosterUpdateCallbacks = [];
  devicesUpdatedCallbacks = [];
  messagingSocket = null;

  messageUpdateCallbacks= [];

  initializeSdkWrapper = async () => {
    this.meetingSession = null;
    this.audioVideo = null;
    this.title = null;
    this.name = null;
    this.region = null;
    this.currentAudioInputDevice = null;
    this.currentAudioOutputDevice = null;
    this.currentVideoInputDevice = null;
    this.audioInputDevices = [];
    this.audioOutputDevices = [];
    this.videoInputDevices = [];
    this.roster = {};
    this.previwCamera = null;
    this.rosterUpdateCallbacks = [];
    this.configuration = null;
    this.messagingSocket = null;
    this.devicesUpdatedCallbacks = [];
    this.messageUpdateCallbacks = [];
    this.meetingSession = null;
    this.audioVideo = null;

  };

  /** Create Meeting */
  createMeet = async (meet_uuid)=> {
    const response = await axios({
      url: `https://development-virtualface.ws.solera.pe/v1/meeting/${meet_uuid}/join`,
      method: 'post',
    })
    let setMeetingResponse = {}
    let setAttendeeResponse = {}
    if (response.data.success) {
      setMeetingResponse = {
        MeetingId: response.data.data.config.meeting.meeting_id,
        MediaPlacement: {
          AudioHostUrl: response.data.data.config.meeting.media_placement.audio_host_url,
          ScreenDataUrl: response.data.data.config.meeting.media_placement.screen_data_url,
          ScreenSharingUrl: response.data.data.config.meeting.media_placement.screen_sharing_url,
          ScreenViewingUrl: response.data.data.config.meeting.media_placement.screen_viewing_url,
          SignalingUrl: response.data.data.config.meeting.media_placement.signaling_url,
          TurnControlUrl: response.data.data.config.meeting.media_placement.turn_control_url
        }
      }
      setAttendeeResponse = {
        AttendeeId: response.data.data.config.attendee.attendee_id,
        ExternalUserId: response.data.data.config.attendee.external_user_id,
        JoinToken: response.data.data.config.attendee.join_token
      }
      this.configuration = new MeetingSessionConfiguration(setMeetingResponse, setAttendeeResponse)
      await this.initializeMeetingSession(this.configuration);
    }
  }

  /** Init Meeting Session */
  initializeMeetingSession = async (
    configuration
  ) => {
    this.previwCamera = document.getElementById('video-element')
    const logger = new ConsoleLogger('SDK', LogLevel.DEBUG);
    const deviceController = new DefaultDeviceController(logger);
    const meetingSession = new DefaultMeetingSession(
      configuration,
      logger,
      deviceController
    );
    this.audioVideo = meetingSession.audioVideo;
    const audioInputDevices = [];
    (await this.audioVideo?.listAudioInputDevices()).forEach(
      (mediaDeviceInfo) => {
        audioInputDevices.push({
          label: mediaDeviceInfo.label,
          value: mediaDeviceInfo.deviceId
        });
      }
    );
    const audioOutputDevices = [];
    (await this.audioVideo?.listAudioOutputDevices()).forEach(
      (mediaDeviceInfo) => {
        audioOutputDevices.push({
          label: mediaDeviceInfo.label,
          value: mediaDeviceInfo.deviceId
        });
      }
    );
    this.publishDevicesUpdated();
    const observer = {
      audioInputsChanged: (freshAudioInputDeviceList) => {
        freshAudioInputDeviceList.forEach((mediaDeviceInfo) => {
          console.log(`Device ID: ${mediaDeviceInfo.deviceId} Microphone: ${mediaDeviceInfo.label}`);
        });
      },
      audioOutputsChanged: (freshAudioOutputDeviceList) => {
        console.log('Audio outputs updated: ', freshAudioOutputDeviceList);
      },
      videoInputsChanged: (freshVideoInputDeviceList) => {
        console.log('Video inputs updated: ', freshVideoInputDeviceList);
      }
    };
    this.audioVideo?.addDeviceChangeObserver(observer);
    this.audioVideo?.realtimeSubscribeToAttendeeIdPresence(
      (presentAttendeeId, present) => {
        if (!present) {
          delete this.roster[presentAttendeeId];
          this.publishRosterUpdate.cancel();
          this.publishRosterUpdate();
          return;
        }
        this.audioVideo?.realtimeSubscribeToVolumeIndicator(
          presentAttendeeId,
          async (
            attendeeId,
            volume,
            muted,
            signalStrength
          ) => {
            const baseAttendeeId = new DefaultModality(attendeeId).base();
            if (baseAttendeeId !== attendeeId) {
              return;
            }
            let shouldPublishImmediately = false;
            if (!this.roster[attendeeId]) {
              this.roster[attendeeId] = { name: '' };
            }
            if (volume !== null) {
              this.roster[attendeeId].volume = Math.round(volume * 100);
            }
            if (muted !== null) {
              this.roster[attendeeId].muted = muted;
            }
            if (signalStrength !== null) {
              this.roster[attendeeId].signalStrength = Math.round(
                signalStrength * 100
              );
            }
            if (shouldPublishImmediately) {
              this.publishRosterUpdate.cancel();
            }
            this.publishRosterUpdate();
          }
        );

      }

    );

    const videoInputs = await this.audioVideo?.listVideoInputDevices();
    if (videoInputs && videoInputs.length > 0 && videoInputs[0].deviceId) {
      this.currentVideoInputDevice = {
        label: videoInputs[0].label,
        value: videoInputs[0].deviceId
      };
      await this.audioVideo?.chooseVideoInputDevice(videoInputs);
    }
  }

  // JoinRoom
  joinRoom = async () => {
    const element = document.getElementById('audioElement')
    const audioInputs = await this.audioVideo?.listAudioInputDevices();
    if (audioInputs && audioInputs.length > 0 && audioInputs[0].deviceId) {
      this.currentAudioInputDevice = {
        label: audioInputs[0].label,
        value: audioInputs[0].deviceId
      };
      await this.audioVideo?.chooseAudioInputDevice(audioInputs[0].deviceId);
    }
    const audioOutputs = await this.audioVideo?.listAudioOutputDevices();
    if (audioOutputs && audioOutputs.length > 0 && audioOutputs[0].deviceId) {
      this.currentAudioOutputDevice = {
        label: audioOutputs[0].label,
        value: audioOutputs[0].deviceId
      };
      await this.audioVideo?.chooseAudioOutputDevice(audioOutputs[0].deviceId);
    }
    this.publishDevicesUpdated();
    this.audioVideo?.bindAudioElement(element);
    this.audioVideo?.start();
  }


  publishDevicesUpdated = () => {
    this.devicesUpdatedCallbacks.forEach((callback) => {
        callback({
          currentAudioInputDevice: this.currentAudioInputDevice,
          currentAudioOutputDevice: this.currentAudioOutputDevice,
          currentVideoInputDevice: this.currentVideoInputDevice,
          audioInputDevices: this.audioInputDevices,
          audioOutputDevices: this.audioOutputDevices,
          videoInputDevices: this.videoInputDevices
        });
      }
    );
  };

  publishRosterUpdate = throttle(() => {
    for (let i = 0; i < this.rosterUpdateCallbacks.length; i += 1) {
      const callback = this.rosterUpdateCallbacks[i];
      callback(this.roster);
    }
  }, ChimeSdkWrapper.ROSTER_THROTTLE_MS);

  chooseVideoInputDevice = async (device) => {
    try {
      await this.audioVideo?.chooseVideoInputDevice(device.value);
      this.currentVideoInputDevice = device;
    } catch (error) {
      this.logError(error);
    }


  };
   logError = (error) => {
    // eslint-disable-next-line
    console.error(error);
  };
  leaveRoom = async (end)=> {
    try {
      this.audioVideo?.stop();
    } catch (error) {
      this.logError(error);
    }

    try {
      await this.messagingSocket?.close(ChimeSdkWrapper.WEB_SOCKET_TIMEOUT_MS);
    } catch (error) {
      this.logError(error);
    }

    // try {
    //   if (end && this.title) {
    //     await fetch(
    //       `${getBaseUrl()}end?title=${encodeURIComponent(this.title)}`,
    //       {
    //         method: 'POST'
    //       }
    //     );
    //   }
    // } catch (error) {
    //   this.logError(error);
    // }

    this.initializeSdkWrapper();
  };


}


/** JOIN MEET/ROOM */

/** JOIN MEET/ROOM MESSAGING */

/** SEND MESSAGE */

/** LEAVE MEET/ROOM */

export default ChimeSdkWrapper;

// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

import React, { Dispatch } from 'react';

import localStorageKeys from '../constants/localStorageKeys.json';
import ClassMode from '../enums/ClassMode';

let classMode =
  localStorage.getItem(localStorageKeys.CLASS_MODE) === 'Teacher'
    ? ClassMode.Teacher
    : ClassMode.Student;
if (!classMode) {
  localStorage.setItem(localStorageKeys.CLASS_MODE, ClassMode.Student);
  classMode = ClassMode.Student;
}

export const initialState = {
  classMode
};

const context = React.createContext([
  initialState,
  () => {}
]);

export default function getUIStateContext() {
  return context;
}

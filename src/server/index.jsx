import Koa from "koa";
import bodyParser from "koa-bodyparser";
import proxy from "koa-proxy";
import serve from "koa-static";
import helmet from "koa-helmet";
import logger from "koa-logger";
import Router from "koa-router";
import routes from "./routes";
// import rewrite from "./middleware/authRewrite";
// import authentication from "./middleware/authentication";

// Initialize `koa-router` and setup a route listening on `GET /*`
// Logic has been splitted into two chained middleware functions
// @see https://github.com/alexmingoia/koa-router#multiple-middleware
const router = new Router();

routes({ router });

// Intialize and configure Koa application
const server = new Koa();

server
  // `koa-helmet` provides security headers to help prevent common, well known attacks
  // @see https://helmetjs.github.io/
  .use(helmet())
  .use(logger())
  .use(serve(process.env.RAZZLE_PUBLIC_DIR))
  // .use(authentication)
  // .use(rewrite)
  .use(
    proxy({
      host: process.env.RAZZLE_API_URL,
      jar: false,
      match: /\/api/,
    })
  )
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

export default server;

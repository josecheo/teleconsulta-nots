import React from "react";
import { StaticRouter } from "react-router-dom";
import { renderToString } from "react-dom/server";
import { ServerStyleSheet } from "styled-components";
import App from "../../App";

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

export function getMarkup(ctx, next) {
  const context = {};
  const sheets = new ServerStyleSheet();
  const markup = renderToString(
    sheets.collectStyles(
      <StaticRouter context={context} location={ctx.url}>
        <App />
      </StaticRouter>
    )
  );
  ctx.state.css = sheets.getStyleTags();
  ctx.state.markup = markup;
  return context.url ? ctx.redirect(context.url) : next();
}

export async function renderMarkup(ctx) {
  const razzleStyles = assets.client.css
    ? `<link rel="stylesheet" href="${assets.client.css}">`
    : "";
  const materialStyles = ctx.state.css
    ? `<style id='jss-ssr'>${ctx.state.css}</style>`
    : "";
  const razzleScripts =
    process.env.NODE_ENV === "production"
      ? `<script src="${assets.client.js}" defer></script>`
      : `<script src="${assets.client.js}" defer crossorigin></script>`;

  ctx.status = 200;
  ctx.body = `
    <!doctype html>
      <html lang="es">
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="UTF8mb4"  />
        <title>Solera | Teleconsulta</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${razzleStyles}
        ${materialStyles}
        ${razzleScripts}
        <meta name="msapplication-TileColor" content="#40bbf5">
        <meta name="theme-color" content="#40bbf5">
      </head>
      <body>
        <div id="root">${ctx.state.markup}</div>
         <audio id='audioElement' style={{ display: 'none' }} />
      </body>
    </html>
  `;
}

import { getMarkup, renderMarkup } from "./middleware/render";
// import { login } from "./api/authentication";

const routes = ({ router }) => {
  router.get("/*", getMarkup, renderMarkup);
  // router.post("/logout", logout);
};

export default routes;

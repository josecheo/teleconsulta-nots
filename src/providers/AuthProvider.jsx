import React, { useReducer, createContext, Dispatch } from "react";
import { authReducer, State, Actions } from "../reducers/authReducer";

const initialState = {
  isLoggedIn: false,
  meet: {},
};

export const AuthStateContext = createContext({});
export const AuthDispatchContext = createContext(() => { });

export const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  return (
    <AuthStateContext.Provider value={state}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
};

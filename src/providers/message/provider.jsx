/* eslint-disable react/require-default-props */
import React, { createContext, useReducer } from 'react';
import PropTypes from 'prop-types';
import messageReducer from './reduce';

const messages = {
  type: '',
  message: '',
};

export const MessageContext = createContext(messages);
export const DispatchMessageContext = createContext(null);

export const MessageProvider = ({ children }) => {
  const [state, dispatch] = useReducer(messageReducer, messages);
  return (
    <DispatchMessageContext.Provider value={dispatch}>
      <MessageContext.Provider value={state}>
        {children}
      </MessageContext.Provider>
    </DispatchMessageContext.Provider>
  );
};

MessageProvider.propTypes = {
  children: PropTypes.element,
};

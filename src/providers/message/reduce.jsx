function MessageReducer(state, action) {
  switch (action.type) {
    case "success":
    case "error":
      return { ...state, ...{ type: action.type, message: action.message } };
    case "reset":
      return {};
    default:
      throw new Error();
  }
}

export default MessageReducer;

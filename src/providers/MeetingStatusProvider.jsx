import React, {
  useContext,
  useRef,
  createContext,
  useState
} from 'react';
import getChimeContext from '../context/getChimeContext';


export const MeetingStatusContext = createContext('loading');

export default function MeetingStatusProvider(props) {
  const chime = useContext(getChimeContext());
  const { children } = props;
  const [meetingStatus, setMeetingStatus] = useState('loading')
  const audioElement = useRef(null);
  const value = {
    meetingStatus,
    setValue: (status) => {
      setMeetingStatus(status);
    },
  }



  // useEffect(() => {
  //   const start = async () => {
  //     try {
  //       await chime?.createMeet();
  //       setMeetingStatus("success");
  //     } catch (error) {
  //       console.error(error);
  //       setMeetingStatus('faild');
  //     }
  //   };
  //   start();
  // }, []);

  return (
    <MeetingStatusContext.Provider value={value}>
      <audio style={{ display: 'none' }} />
      {children}</MeetingStatusContext.Provider>
  );
};

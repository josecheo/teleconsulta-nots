import React, { createContext, useState } from "react";

export const MeetingSessionContext = createContext(true);

export const MeetingProvider = ({ children }) => {
  const [globalMeetingsession, setSeetingsession] = useState();

  const value = {
    globalMeetingsession,
    setValue: (ev) => {
      setSeetingsession(ev);
    },
  };

  return (
    <MeetingSessionContext.Provider value={value}>{children}</MeetingSessionContext.Provider>
  );
};

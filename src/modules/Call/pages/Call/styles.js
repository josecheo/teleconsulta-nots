import styled, { css } from "styled-components";

export const Wrapper = styled.div`
  display: grid;
  grid-template-rows: 140px 1fr 90px;
  grid-template-columns: minmax(15%, 16%) 1fr ${(props) =>
    props.openChat ? ".4fr" : "5%"};
  grid-gap: 8px;
  max-width: 100%;
  height: 100vh;
  align-items: center;
  background: #ededed;
  @media (max-width: 1024px) {
    ${(props) =>
    props.openChat &&
    css`
        display: grid;
        grid-template-rows: 140px 1fr 90px;
        grid-template-columns: 0.15fr 1fr;
      `};
  }
  @media (max-width: 800px) {
    grid-template-columns: 0.2fr 1fr;
  }

  @media (max-width: 655px) {
    display: grid;
    grid-auto-columns: 1fr;
    grid-template-rows: 1fr;
  }
`;
export const ContainerImg = styled.img`
  display: flex;
  width: 100%;
  height: 100%;
  background-size: contain;

  /* justify-content: center;
  align-items: center; */
  /* padding: 0.5rem 0.5rem; */
  @media (max-width: 800px) {
    padding-left: 30px;
    justify-content: flex-start;
  }
  @media (max-width: 655px) {
    position: fixed;
    top: 2%;
    left: 1%;
    width: 170px;
    height: 150px;
    opacity: 0.3;
  }
`;

// export const StyledLogo = styled.div`
//   display: flex;
//   @media (max-width: 1281px) {
//     height: 80px;
//     width: 120px;
//   }
// `;
// export const Img = styled.img`
//   width: 100%;
//   height: 100%;
//   background-size: container;
//   background-repeat: no-repeat;
// `;

export const Message = styled.div`
  display: grid;
  padding: 0.5rem 0.5rem;
  @media (max-width: 655px) {
    display: none;
  }
`;

export const Lastappointment = styled.h5`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 28px;
  color: #9e9e9e;
`;

export const MessageRow = styled.div`
  &:not(:last-of-type) {
    margin-bottom: 1.5rem;
  }
`;

export const Title = styled.h4``;

export const Subtitle = styled.h5``;

export const ContainerChat = styled.div`
  display: grid;
  justify-self: stretch;
  align-self: stretch;
  height: 100%;
  grid-column-start: 3;
  grid-column-end: 4;
  grid-row-start: 1;
  grid-row-end: 4;
  padding: 0.5rem 0.5rem;
  background: ${(props) => (props.openChat ? "#FFFFFF" : null)};
  @media (max-width: 1024px) {
    ${(props) =>
    props.openChat &&
    css`
        display: flex;
        position: absolute;
        background-color: rgba(255, 255, 255, 0.3);
        right: 0;
        top: 0;
        /* z-index: 1000; */
      `};
  }
  @media (max-width: 800px) {
    ${(props) =>
    !props.openChat &&
    css`
        display: none;
      `};
  }
  @media (max-width: 727px) {
    width: 34%;
  }
  @media (max-width: 655px) {
    width: 100%;
    background-color: rgba(255, 255, 255, 0.6);
  }
`;

export const ContainerSmallCamera = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: flex-end;
  padding: 0.5rem 0.5rem;
  @media (max-width: 800px) {
    position: fixed;
    left: -30%;
    bottom: 120px;
  }
  /* @media (max-width: 655px) {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: blue;
  } */
`;

export const SmallCamera = styled.img`
  width: 192px;
  height: 192px;
  background:#E6E6E6;
  border-radius: 24px;
  @media (max-width: 1281px) {
    height: 130px;
    width: 150px;
  }
  @media (max-width: 655px) {
    position: fixed;
    height: 180px;
    right: 10%;
    top: 5%;
    background-color: blue;
  }
`;

export const ContainerLargeCamera = styled.div`
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: flex-end;
  
  @media (max-width: 800px) {
    grid-column-start: 1;
    grid-column-end: 3;
  }
`;
export const LargeCamera = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 24px;
  background-size: contain;
  background-repeat: no-repeat;
`;

export const ControlsGroup = styled.div`
  display: grid;
  grid-template-columns: 1fr 0.67fr;
  grid-column-start: 2;
  grid-column-end: 3;
  width: 100%;
  @media (max-width: 1024px) {
    ${(props) =>
    props.openChat &&
    css`
        display: grid;
        grid-template-columns: 30% 70%;
      `};
  }
  @media (max-width: 800px) {
    grid-template-columns: 0.4fr 1fr;
    padding-left: 40px;
    grid-column-start: 1;
    grid-column-end: 3;
  }
  @media (max-width: 655px) {
    display: flex;
    width: 100%;
    position: fixed;
    /* left: 24%; */
    bottom: 7%;
  }
  /* @media (max-width: 655px) {
    position: fixed;
    left: 20%;
    bottom: 7%;
  } */
`;

export const Controls = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  @media (max-width: 1024px) {
    ${(props) =>
    props.openChat &&
    css`
        justify-content: center;
      `};
  }
  @media (max-width: 800px) {
    justify-content: flex-start;
  }
  @media (max-width: 655px) {
    display: flex;
    width: 100%;
    justify-content: center;
  }
`;

export const SecondControls = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  @media (max-width: 1024px) {
    ${(props) =>
    props.openChat &&
    css`
        justify-content: flex-start;
      `};
  }
  @media (max-width: 800px) {
    justify-content: flex-start;
  }
  @media (max-width: 655px) {
    display: flex;
    width: 100%;
    justify-content: center;
  }
`;

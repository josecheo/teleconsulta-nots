import React, { useState, useContext, useEffect, useRef } from "react";
import PropTypes, { InferProps } from "prop-types";
import ButtonIcon from "../../../../components/ButtonIcon";
import Logo from "../../../../../public/images/Logo.png";
import capture from "../../../../../public/images/capture1.jpg";
import captureSmall from "../../../../../public/images/capture2.jpg";
import getChimeContext from '../../../../context/getChimeContext';
import MeetingStatus from "../../../../enums/MeetingStatus";
import {
  ContainerImg,
  Wrapper,
  StyledLogo,
  Img,
  Lastappointment,
  Message,
  MessageRow,
  Title,
  Subtitle,
  SmallCamera,
  ContainerSmallCamera,
  ContainerLargeCamera,
  LargeCamera,
  ContainerChat,
  Controls,
  SecondControls,
  ControlsGroup,
} from "./styles";
import Chat from "../../../../components/Chat";
import ChimeSdkWrapper from "../../../../chime/ChimeSdkWrapper";
import VideoLocal from '../../../../components/VideoLocal';
import VideoRemote from '../../../../components/VideoRemote'
import { MeetingStatusContext } from '../../../../providers/MeetingStatusProvider'
import { useHistory } from "react-router-dom";
import { DefaultModality, AudioVideoFacade, DefaultVideoTile, } from "amazon-chime-sdk-js";
import { string } from 'prop-types';
import ContainerVideo from '../../../../components/ContainerVideo/index';

// type InferPropTypes<
//   PropTypes,
//   DefaultProps = {},
//   Props = InferProps<PropTypes>
//   > = {
//     [Key in keyof Props]: Key extends keyof DefaultProps
//     ? Props[Key] | DefaultProps[Key]
//     : Props[Key];
//   };




const VideoStatus = {
  Disabled: 0,
  Loading: 1,
  Enabled: 2
}

function Call(props) {
  const { title, price } = props;
  const audioElement = useRef(null);
  const [openChat, setOpenChat] = useState(false);
  const { meetingStatus, setValue } = useContext(MeetingStatusContext)
  const [disabledCamera, setDisabledCamera] = useState(true)
  const [muted, setMuted] = useState(false);
  const [videoStatus, setVideoStatus] = useState(VideoStatus.Disabled);
  const history = useHistory();
  const [contentShareStream, setcontentShareStream] = useState(false)
  let audioVideo = null;



  const OpenClosetChat = () => {
    setOpenChat(!openChat);
  };

  const chime = useContext(getChimeContext());

  useEffect(() => {
    const start = async () => {
      if (meetingStatus === 'success') {
        try {
          await chime.joinRoom()

          setValue('joinRoom')
        } catch (error) {
          console.error(error);
        }
      };
    }
    start();
  }, [meetingStatus]);


  useEffect(() => {
    const callback = (localMuted) => {
      setMuted(localMuted);
    };
    chime?.audioVideo?.realtimeSubscribeToMuteAndUnmuteLocalAudio(callback);
    return () => {
      if (chime && chime?.audioVideo) {
        chime?.audioVideo?.realtimeUnsubscribeToMuteAndUnmuteLocalAudio(
          callback
        );
      }
    };
  }, []);

  const handleShareMediaStream = async () => {
    await chime?.audioVideo?.startContentShareFromScreenCapture();
    // setcontentShareStream(true)
  }

  return (
    <Wrapper openChat={openChat}>
      <ContainerImg src={Logo} alt="Logo">
      </ContainerImg>

      <Message>
        <MessageRow>
          <Subtitle>Asesor</Subtitle>
          <Title>Javier Eduardo Lecca Cruzado</Title>
        </MessageRow>
        <MessageRow>
          <Subtitle>Observacines:</Subtitle>
          <Lastappointment>Última cita - 28/04/2020 </Lastappointment>
        </MessageRow>
      </Message>
      <ContainerChat openChat={openChat}>
        {openChat && <Chat openChat={OpenClosetChat} />}
      </ContainerChat>

      <ContainerSmallCamera>
        <VideoLocal disabledCamera={disabledCamera} />

      </ContainerSmallCamera>
      <ContainerLargeCamera>
        {/* {meetingStatus === 'joinRoom' && (
          <VideoRemote />
        )} */}
        {meetingStatus === 'joinRoom' && (
          <ContainerVideo openChat={openChat} />
        )}

      </ContainerLargeCamera>
      <ControlsGroup openChat={openChat}>
        <Controls openChat={openChat}>
          <ButtonIcon
            mr={10}
            iconName="video"
            tooltip={false}
            label="Habilitar cámara"
            bg={videoStatus === VideoStatus.Disabled ? 'gray' : 'danger'}
            onClick={async () => {
              // Adds a slight delay to close the tooltip before rendering the updated text in it
              await new Promise(resolve => setTimeout(resolve, 10));
              if (videoStatus === VideoStatus.Disabled) {
                setVideoStatus(VideoStatus.Loading);
                try {
                  if (!chime?.currentVideoInputDevice) {
                    throw new Error('currentVideoInputDevice does not exist');
                  }
                  await chime?.chooseVideoInputDevice(
                    chime?.currentVideoInputDevice
                  );
                  chime?.audioVideo?.startLocalVideoTile();
                  setVideoStatus(VideoStatus.Enabled);
                } catch (error) {
                  // eslint-disable-next-line
                  console.error(error);
                  setVideoStatus(VideoStatus.Disabled);
                }
              } else if (videoStatus === VideoStatus.Enabled) {
                setVideoStatus(VideoStatus.Loading);
                chime?.audioVideo?.stopLocalVideoTile();
                setVideoStatus(VideoStatus.Disabled);
              }
            }}
          />

          <ButtonIcon
            mr={10}
            iconName="leave"
            tooltip={false}
            label="Habilitar cámara"
            bg="danger"
            onClick={() => {
              chime?.leaveRoom(true);
              history.push('/finishCall');
            }}
          />

          <ButtonIcon
            iconName="microphone"
            tooltip={false}
            label="Activar Microfono"
            bg={muted ? 'danger' : 'gray'}
            onClick={async () => {
              if (muted) {
                chime?.audioVideo?.realtimeUnmuteLocalAudio();
              } else {
                chime?.audioVideo?.realtimeMuteLocalAudio();
              }
              // Adds a slight delay to close the tooltip before rendering the updated text in it
              await new Promise(resolve => setTimeout(resolve, 10));
            }}
          >
          </ButtonIcon>
        </Controls>
        <SecondControls openChat={openChat}>
          <ButtonIcon
            mr={10}
            iconName="share"
            tooltip={false}
            label="Activar audio"
            onClick={handleShareMediaStream}
          />
          <ButtonIcon

            iconName="chat"
            onClick={() => setOpenChat(!openChat)}
            tooltip={false}
            label="Abrir Chat"
          />

        </SecondControls>
      </ControlsGroup>
    </Wrapper>
  );
}



export default Call;

import Call from './pages/Call';

const path = '/call'

const routes = [
  {
    name: 'Home',
    path,
    component: Call,
    exact: true,
  }
]

export default routes

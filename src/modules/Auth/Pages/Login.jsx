import React, { useState, useContext, ChangeEvent, FormEvent } from "react";
import { Redirect } from "react-router-dom";
import TextField from "../../../components/TextField";
import Button from "../../../components/Button";
import { Hero, Form, FieldWrap } from "./styles";
import {
  AuthStateContext,
  AuthDispatchContext,
} from "../../../providers/AuthProvider";



const initialForm = {
  uuid: "",
  user: "",
};

const Login= () => {
  const dispatch = useContext(AuthDispatchContext);
  const { isLoggedIn } = useContext(AuthStateContext);
  const [form, setForm] = useState(initialForm);
  if (isLoggedIn) {
    return <Redirect to={{ pathname: "/meet" }} />;
  }

  const handleChange = (ev) => {
    const { name, value } = ev.target;
    setForm({ ...form, [name]: value });
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();
    dispatch({
      type: "LOGIN",
      meet: {
        uuid: form?.uuid,
        user: form?.user,
      },
    });
  };

  return (
    <>
      <Hero bg="gray" color="white" py={6}>
        WELCOME TO TELECONSULTA
      </Hero>
      <Form mx="auto" p={5} width={1 / 3} onSubmit={handleSubmit}>
        <FieldWrap>
          <TextField
            type="text"
            name="uuid"
            value={form?.uuid}
            placeholder="uuid de la sala"
            required
            onChange={handleChange}
          />
        </FieldWrap>
        <FieldWrap>
          <TextField
            type="text"
            name="user"
            value={form?.user}
            placeholder="Su nombre"
            required
            onChange={handleChange}
          />
        </FieldWrap>
        <FieldWrap>
          <Button type="submit" bg="gray" label='Entrar'>

          </Button>
        </FieldWrap>
      </Form>
    </>
  );
};

export default Login;

import Login from "./Pages/Login";

const path = "/";

const routes = [
  {
    name: "Iniciar sesión",
    path,
    component: Login,
    exact: true,
    isProtected: false,
  },
];

export default routes;

import Home from "./Pages";

const path = "/meet";

const routes = [
  {
    name: "Home",
    path,
    component: Home,
    exact: true,
  },
];

export default routes;

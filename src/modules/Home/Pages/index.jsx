import React, { useEffect, useRef, useState, useContext } from "react";
import Logo from "../../../../public/Logo.png";
import Button from "../../../components/Button";
import Camera from "../../../components/Camera";
import Icon from "../../../components/Icons";
import getChimeContext from '../../../context/getChimeContext';
import { MeetingStatusContext } from '../../../providers/MeetingStatusProvider'

import {
  Container,
  ContainerButton,
  ContainerCamera,
  Hero,
  Info,
  PrincipalText,
  SecondText,
  StyledLogo,
  Wrapper,
  Img,
} from "./styles";
import { Redirect } from "react-router-dom";
import {
  AuthStateContext,
} from "../../../providers/AuthProvider";





const Home = () => {
  const [toCall, setTocall] = useState(false);
  const chime = useContext(getChimeContext());
  const { meetingStatus, setValue } = useContext(MeetingStatusContext)
  const { meet } = useContext(AuthStateContext);

  useEffect(() => {
    const start = async () => {
      try {
        await chime?.createMeet(meet.uuid);
        setValue("success");
      } catch (error) {
        console.error(error);
        setValue('faild');
      }
    };
    start();
  }, []); 




  useEffect(() => {
    const previwCamera = document.getElementById('video-element')
    const start = async () => {
      try {
        chime?.audioVideo?.startVideoPreviewForVideoInput(previwCamera)
      } catch (error) {
        console.error(error);
      }
    };
    start();

  }, [meetingStatus]);

  const handleStartCall = () => {
    const previwCamera = document.getElementById('video-element')
    try {
      chime?.audioVideo?.stopVideoPreviewForVideoInput(previwCamera)
    } catch (error) {
      console.error(error);
    }

    setTimeout(() => {
      setTocall(true)
    }, 1000);

  };




  return (
    <Wrapper>
      <Hero>
        <StyledLogo>
          <Img src={Logo} alt="Logo" />
        </StyledLogo>
      </Hero>
      <Container>
        <ContainerCamera>
          <Camera id='video-element'></Camera>
        </ContainerCamera>
        <Info>
          <PrincipalText>
            Bienvenidos a nuestos nuevo servicio de Teleconsultas
          </PrincipalText>
          <SecondText>Esperando a Jose Alvarez</SecondText>
          <ContainerButton>
            <Button
              onClick={handleStartCall}
              renderIcon={
                <Icon color="white" name="video" width={20} height={20} />
              }
              label="Ingresar"
            >
              PRUEBA
            </Button>

          </ContainerButton>
        </Info>
      </Container>
      {toCall && <Redirect to="/call" />}
    </Wrapper >
  );
};

export default Home;

import styled from 'styled-components';
import { color, layout, space } from 'styled-system';

export const Wrapper = styled.div`
  display: grid;
  grid-template-rows: 120px 1fr;
  grid-gap: 8px; 
  width: 100%;
  height: 100vh;
  align-items: center;
  padding:8px;
    @media (max-width: 480px) {
     grid-template-rows: 1fr 1fr;
      /* align-items: flex-start;
   grid-template-columns: 1fr; */
  }

    /* justify-content: center; */
`;
export const Hero = styled.div`
display: flex;
padding: 20px 40px;
  @media (max-width: 943px) {
   justify-content: center;
  }
      @media (max-width: 480px) {
     padding: 10px 10px;
  }
`;

export const StyledLogo = styled.div`
display: flex;
`;

export const Img = styled.img`
width:100%;
`;

export const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr .48fr;
  padding:8px;
  grid-gap: 8px; 
  height: 100%;
  align-items: center;
  background-color:#FFF;
  @media (max-width: 943px) {
      align-items: flex-start;
   grid-template-columns: 1fr;
  }
`;
export const Info = styled.div`
  display: flex;
  height: 170px;
  flex-direction: column;
  align-items: center;
  padding:8px;
  justify-content: space-between;
    @media (max-width: 943px) {
  align-self: center;
    @media (max-width: 480px) {
      display:flex;
        height: 100%;
      flex-direction:column;
     justify-content: space-between;

  }

  }
`;
export const ContainerCamera = styled.div`
display: flex;
width: 644px;
height: 420px;
justify-self: flex-end;
  @media (max-width: 987px) {
   grid-template-columns: 1fr;
   justify-self: center;
   max-width: 744px;
    max-height: 360px;
  }
    @media (max-width: 480px) {
      display:none;
      /* align-items: flex-start;
   grid-template-columns: 1fr; */
  }
`;

export const PrincipalText = styled.div`
width: 300px;
font-family: ${(props) => props.theme.fonts.secondary};
font-style: normal;
font-weight: 700;
font-size: 21px;
line-height: 28px;
text-align: center;
color:${(props) => props.theme.colors.primary};
      @media (max-width: 480px) {
        font-size: 28px;
  }


`;

export const SecondText = styled.div`
width: 276px;
text-align: center;
font-family: ${(props) => props.theme.fonts.secondary};
font-style: normal;
font-weight: 700;
font-size: 16px;
line-height: 28px;
color:${(props) => props.theme.colors.primary};

    @media (max-width: 480px) {
        display:none;
      /* align-items: flex-start;
   grid-template-columns: 1fr; */
  }
`;

export const ContainerButton = styled.div`
display: flex;
width: 192px;
height: 48px;
    @media (max-width: 480px) {
    width: 80%;
    height: 68px;
      /* align-items: flex-start;
   grid-template-columns: 1fr; */
  }
`;
import React from 'react';
import Chat from '../../../../components/Chat';
import { Wrapp, ContainerChat } from './styles'




function Chats(props) {

  return (
    <Wrapp>
      <ContainerChat>
        <Chat />
      </ContainerChat>
    </Wrapp>
  );
}

export default Chats;

import styled from "styled-components";

export const Wrapp = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  height: 100vh;
  background: #ededed;
`;

export const ContainerChat = styled.div`
  display: flex;
  width: 368px;
  height: 100%;
`;

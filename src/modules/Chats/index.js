import Chats from './pages/Chats';

const path = '/chats'

const routes = [
  {
    name: 'chats',
    path,
    component: Chats,
    exact: true,
  }
]

export default routes

import React from "react";
import { Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import theme from "./styles/theme";
import GlobalStyle from "./styles/global";
import RouterOutlet from "./components/RouterOutlet";
import routes from "./routes";

const App = () => (

  <ThemeProvider theme={theme}>

    <GlobalStyle />
    <Switch>
      {routes.map((route) => (
        <RouterOutlet key={route.path} {...route} />
      ))}
    </Switch>

  </ThemeProvider >
);

export default App;

import styled, { css } from "styled-components";
import { variant, color, background, space, margin } from "styled-system";

const received = variant({
  prop: "received",
  variants: {
    true: {
      display: "flex",
      // margin: "5px",
      color: "#FFFFFF",
      padding: "10px 10px",
      borderRadius: "10px",
      alignSelf: "flex-end",
      maxWidth: "250px",
      textAlign: "left",
    },
    false: {
      display: "flex",
      // margin: "5px",
      color: "#000000",
      textAlign: "left",
      padding: "10px 10px",
      borderRadius: "10px",
      alignSelf: "flex-start",
      maxWidth: "250px",
    },
  },
});

export const ContainerCard = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  word-wrap: break-word;
  word-break: break-word;
`;

export const ContainerFIle = styled.div`
  display: flex;
  border-radius: 3px;
  width: 200px;
  cursor: pointer;
  height: 40px;
  justify-content: space-evenly;
  align-items: center;

  ${(props) =>
    props.received &&
    css`
      background: rgba(244, 244, 244, 0.3);
    `};

  ${(props) =>
    !props.received &&
    css`
      background: rgba(0, 0, 0, 0.1);
    `};
`;

export const Download = styled.div`
  display: flex;
  border-radius: 50%;
  /* right: 3px;
  top: 3px; */
`;

// export const ImgPrev = styled.img`
//   display: flex;
//   border-radius: 10px;
//   width: 100%;
// `;
export const WrappCard = styled.div`
  display: flex;
  flex-direction: column;
  ${margin};
  ${(props) =>
    props.received &&
    css`
      background: ${(props) => props.theme.colors.primary};
    `};
  ${(props) =>
    !props.received &&
    css`
      background: ${(props) => props.theme.colors.secondary};
    `};
  ${(props) =>
    props.wait &&
    css`
      opacity: 0.1;
    `};
  ${received};
`;

export const Spam = styled.span`
  ${margin};
`;
import PropTypes, { InferProps } from "prop-types";
import React from "react";
import ButtonIcon from "../ButtonIcon/index";
import {
  ContainerCard,
  WrappCard,
  ContainerFIle,
  Download,
  Spam,
} from "./styles";


// type InferPropTypes<
//   PropTypes,
//   DefaultProps = {},
//   Props = InferProps<PropTypes>
//   > = {
//     [Key in keyof Props]: Key extends keyof DefaultProps
//     ? Props[Key] | DefaultProps[Key]
//     : Props[Key];
//   };

const propTypes = {
  received: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  wait: PropTypes.bool.isRequired,
  file: PropTypes.string.isRequired,
};

const defaultProps = {
  received: true,
  text: "",
  wait: false,
  file: "",
};

// type Props = InferPropTypes<typeof propTypes, typeof defaultProps>;

function Message(props) {
  const { received, text, mb, file, wait } = props;
  const handleDownload = () => {
    const filedownload = file;
    window.open(filedownload, "_top");
  };
  return (
    <WrappCard draggable="false" mb={mb} wait={wait} received={received}>
      {file && (
        <ContainerFIle onClick={handleDownload} received={received}>
          {wait ? "Subiendo archivo" : "Descargar archivos"}
          <Download>
            <ButtonIcon
              size={"small"}
              bg={"transparent"}
              iconName="download"
              tooltip={false}
              label="download"
            />
          </Download>
        </ContainerFIle>
      )}
      {/* <ImgPrev src={file} alt="as" /> */}
      {text && (
        <ContainerCard
          draggable="false"
          mb={mb}
          wait={wait}
          received={received}
        >
          <Spam mt={file ? '10px' : '0px'} draggable="false">{text}</Spam>
        </ContainerCard>
      )
      }
    </WrappCard >
  );
}

Message.defaultProps = defaultProps;

export default Message;

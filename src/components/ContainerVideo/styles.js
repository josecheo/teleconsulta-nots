import styled, { css } from 'styled-components';
import { height, layout, space, width } from 'styled-system';

export const Wrapper = styled.div`
display:flex;
width: auto;
justify-content:center;
align-content: center;
height:680px;
background:#E6E6E6;
border-radius: 24px;
position: relative;
${(props) => props.activeVideo &&
        css`
        background-color:transparent;
    `};
`;


export const PrincipalVideo = styled.video`
${(props) =>
        props.changeStyled &&
        css`
width:250px;
height:150px;
border-radius:10px;
position: absolute;
top:30px;
right:200px;
z-index: 20;
    `};
border-radius:24px;
${(props) =>
        !props.changeStyled &&
        css`
width:100%;
z-index: 19;
position: relative;
border-radius:24px;
    `};
`;

export const SecondVideo = styled.video`
${(props) =>
        props.changeStyled &&
        css`
width:250px;
height:150px;
border-radius:10px;
position: absolute;
top:30px;
right:200px;
z-index: 20;
`};
border-radius:24px;
${(props) =>
        !props.changeStyled &&
        css`
width:100%;
border-radius:24px;
z-index: 19;
position: relative;
`};
`;

export const ContainerButton = styled.div`
display:flex;
position: absolute;
bottom: 20px;
right: 160px;
z-index: 21;

`;


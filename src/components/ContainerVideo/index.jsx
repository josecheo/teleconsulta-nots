// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
import React, { useContext, useEffect, useRef, useState } from 'react';
import getChimeContext from '../../context/getChimeContext';
import { Wrapper, PrincipalVideo, SecondVideo, ContainerButton } from './styles';
import { DefaultModality } from 'amazon-chime-sdk-js';
import ButtonIcon from '../ButtonIcon/index'


function ContainerVideo(props){
  const { openChat } = props
  const chime = useContext(getChimeContext());
  const PrincipalElement = useRef(null)
  const SecondElement = useRef(null)
  const [changeStyled, setChangeStyled] = useState(false)
  const [activeVideo, setActiveVideo] = useState(false)

  useEffect(() => {
    const videoElementStack= [PrincipalElement.current, SecondElement.current];
    const tileMap = {};
    const observer = {
      videoTileDidUpdate: (tileState) => {
        if (tileState.boundAttendeeId || tileState.isContent) {
          const yourAttendeeId = chime?.configuration?.credentials?.attendeeId;
          const boundAttendeeId = tileState.boundAttendeeId;
          const baseAttendeeId = new DefaultModality(boundAttendeeId).base();
          if (baseAttendeeId !== yourAttendeeId) {
            const videoElement = tileMap[tileState.tileId] || videoElementStack.pop();
            if (videoElement) {
              tileMap[tileState.tileId] = videoElement;
              chime?.audioVideo?.bindVideoElement(tileState.tileId, videoElement);
              setActiveVideo(true)
            }
          } else {
            console.log("Yo estoy compartiendo pantalla ahora")
            setActiveVideo(false)
          }
        } else {
          setActiveVideo(false)
        }
      },
      videoTileWasRemoved: (tileId) => {
        const videoElement = tileMap[tileId];
        if (videoElement) {
          videoElementStack.push(videoElement);
          delete tileMap[tileId];
        }
        // setActiveVideo(false)
      }

    };
    chime?.audioVideo?.addObserver(observer);
    setActiveVideo(false)
  }, [])


  return (
    <Wrapper activeVideo={activeVideo}>
      <PrincipalVideo ref={PrincipalElement} muted changeStyled={!changeStyled} />
      <SecondVideo ref={SecondElement} muted changeStyled={changeStyled} />
      {activeVideo && (
        <ContainerButton>
          <ButtonIcon
            activeVideo={activeVideo}
            mr={10}
            iconName="rotate"
            tooltip={false}
            label="Habilitar cámara"
            bg="transparent"
            onClick={() => {
              setChangeStyled(!changeStyled)
            }}
          />
        </ContainerButton>
      )}



    </Wrapper>
  );
}
export default ContainerVideo
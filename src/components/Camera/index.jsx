import PropTypes, { InferProps } from 'prop-types';
import React from 'react';
import { Wrapper } from './styles';

const propTypes = {
  text: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

const defaultProps = {
  text: '',
  src: '',
  id: '',
};

function Camera(props) {

  return (
    <Wrapper
      id={props.id}
      muted
    >
    </Wrapper>
  );
}

Camera.defaultProps = defaultProps;

export default Camera;
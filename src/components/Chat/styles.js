import styled, { css } from "styled-components";
import { height, layout, space, width } from "styled-system";

export const Form = styled.form`
  display: grid;
  position: relative;
  grid-template-rows: 30px 1fr 80px;
  grid-gap: 8px;
  width: 100%;
  height: 100%;
`;
export const OverlayHidden = styled.div`
  display: grid;
  position: absolute;
  width: 100%;
  height: 100%;
`;

export const Exit = styled.div`
  display: flex;
  justify-self: flex-end;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  margin-right: 5px;
  margin-top: 1px;
  width: 30px;
  height: 30px;
  transition: background 0.1s linear 0.1s;
  &:hover {
    cursor: pointer;
    background: #f4f4f4;
  }
`;

export const ExitVisor = styled.div`
  display: none;
  justify-self: flex-end;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  margin-right: 5px;
  margin-top: 1px;
  width: 30px;
  height: 30px;

  transition: background 0.1s linear 0.1s;
  &:hover {
    cursor: pointer;
    background: #f4f4f4;
  }
  ${(props) =>
    props.open &&
    css`
      display: flex;
    `}
`;

export const ContainerMessages = styled.div`
  display: flex;
  /* position: relative; */

  max-height: 80vh;
  overflow-y: auto;
  flex-direction: column-reverse;
  /* padding: 15px; */
`;

export const ContainerSendMessage = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 8px;
  align-items: center;
  border: 1px solid #f4f4f4;
`;

export const Text = styled.div`
  display: flex;
  max-width: 90px;
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  font-family: ${(props) => props.theme.fonts.primary};
  line-height: 21px;
  text-align: center;
  color: #000000;
  opacity: 0.5;
`;

export const ContainerInput = styled.div`
  display: flex;
  appearance: none;
  width: 90%;
  justify-content: flex-start;
  padding: 0 8px;
  align-items: center;
`;

export const ConInput = styled.div`
  display: flex;
  justify-content: space-between;
  height: 45px;
  width: 80%;
  margin-left: 8px;
  border-radius: 22px;
  padding: 0.75rem 1rem;
  border: 1px solid rgba(0, 0, 0, 0.12);
  transition: box-shadow 0.3s;
  &:focus-within {
    box-shadow: 1px 1px 5px -2px rgba(0, 0, 0, 0.85);
  }
`;

export const Input = styled.textarea`
  width: 90%;
  word-wrap: break-word;
  word-break: break-word;
  appearance: none;
  background-color: transparent;
  border: 0;
  font-family: ${(props) => props.theme.fonts.primary};
  font-style: normal;
  font-weight: 500;
  font-size: 0.8rem;
  color: rgba(0, 0, 0, 0.7);
`;

export const ContainerButtonFile = styled.div`
  position: relative;
  /* right: 17px;
  top: 11px; */
`;

export const ContainerEmoji = styled.div`
  display: none;
  position: absolute;
  width: 0px;
  animation-duration: 0.3s;
  animation-name: aparecer;
  ${(props) =>
    props.open &&
    css`
      display: flex;
      bottom: 60px;
      width: 260px;
      opacity: 1;
      @keyframes aparecer {
        from {
          width: 0px;
          opacity: 0;
        }

        to {
          width: 260px;
          opacity: 1;
        }
      }
    `};
`;

export const ContainerSelectTypeFile = styled.div`
  display: none;
  position: absolute;
  bottom: 40px;
  right: -20px;
  width: 60px;
  opacity: 1;
  background-color: rgba(255, 255, 255, 0.3);
  box-shadow: 1px 1px 5px -2px rgba(0, 0, 0, 0.85);
  animation-duration: 0.3s;
  animation-name: aparecerType;
  ${(props) =>
    props.open &&
    css`
      display: flex;
      flex-direction: column;
      padding: 10px;
      border-radius: 5px;
      @keyframes aparecerType {
        from {
          height: 0px;
          opacity: 0;
        }
        to {
          height: 170px;
          opacity: 1;
        }
      }
    `};
`;


export const InputFile = styled.input`
display:none;
`;

export const ImgTypeFile = styled.img`
  cursor: pointer;
  margin-top: 10px;
  margin-bottom: 10px;
  width: 40px;
  height: 40px;
  &:hover {
    transform: scale(1.07);
  }
`;
export const OverlayVisor = styled.div`
  display: none;
  position: absolute;
  
  /* background-color:red; */
  /* background: ${(props) => props.theme.colors.secondary}; */
  background: transparent;
  width: 100%;
  height: 100%;
 
  ${(props) =>
    props.open &&
    css`
      display: flex;
      z-index: 1000;
    `};
  ${(props) => props.hidden && css``};
    
`;

export const VisorFile = styled.div`
  display: none;
  position: absolute;
  background: ${(props) => props.theme.colors.secondary};
  width: 100%;
  height: 100%;
  z-index: 500;
  padding:20px;
  animation-duration: 0.23s;
  animation-name: aparecerVisorFile;
  flex-direction: column;
  border: ${(props) => props.theme.colors.primary} 5px dashed;
  @keyframes aparecerVisorFile {
    from {
      top: 500px;
    }
    to {
      top: 0px;
    }
  }
  ${(props) =>
    props.open &&
    css`
      display: flex;
    `};
`;
export const ImgVisor = styled.img`
display:flex;
justify-self:center;
align-self:center;
justify-content:center;
align-items: center;

`;



export const SendVisor = styled.div`
  display: flex;
  width: 100%;
  padding: 0.4rem;
  justify-content: space-evenly;
  align-items: center;
  background-color: transparent;
`;

export const VisorFleInput = styled.input`
  width: 100%;
  appearance: none;
  background-color: transparent;
  border: 0;
  font-family: ${(props) => props.theme.fonts.primary};
  font-style: normal;
  font-weight: 500;
  font-size: 0.8rem;
  color: rgba(0, 0, 0, 0.7);
`;

export const Line = styled.div`
  display: flex;
  width: 100%;
  height: 3px;
  background: ${(props) => props.theme.colors.primary};
`;

export const ContainerImg = styled.div`
  display: flex;
  overflow-y: auto;
  justify-content: center;
  padding: 2px;
  max-width: 100%;
`;
export const ImgPrev = styled.img`
  display: flex;
  margin: 1px;
  max-width: 22%;
  border: 1 white solid;
`;

export const MessageDrop = styled.div`
  display: flex;

  background-color: red;
`;

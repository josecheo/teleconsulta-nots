import PropTypes, { InferProps, number, array } from "prop-types";
import React, { useState, useEffect, useRef, useContext } from "react";
import ButtonIcon from "../ButtonIcon/index";
import Icon from "../Icons/index";
import useFetch from "../../services/useFetch";
import moment from "moment";
import Message from "../Message/index";
import imgGaleria from "../../../public/images/galeria.svg";
import imgPapel from "../../../public/images/papel.svg";
import useSubmitForm from "../../services/useSubmitForm";
import anadir from "../../../public/images/anadir.svg";
import "emoji-mart/css/emoji-mart.css";
import { Picker, Emoji } from "emoji-mart";

import {
  ContainerInput,
  ContainerMessages,
  SendVisor,
  VisorFile,
  ContainerSendMessage,
  OverlayVisor,
  ContainerEmoji,
  Exit,
  ExitVisor,
  ImgTypeFile,
  OverlayHidden,
  ImgVisor,
  ContainerImg,
  InputFile,
  ImgPrev,
  ContainerSelectTypeFile,
  Input,
  ContainerButtonFile,
  Line,
  VisorFleInput,
  ConInput,
  MessageDrop,
  Form,
} from "./styles";
import socketIOClient from "socket.io-client";
import {
  AuthStateContext,
  AuthDispatchContext,
} from "../../providers/AuthProvider";

moment.locale("es");
const propTypes = {
  text: PropTypes.string.isRequired,
};

const defaultProps = {
  text: "",
};

const initialForm = {
  text: "",
  user_code: "",
  id_chat: 1,
  room_chat: "",
  file_path: "",
  id_web_message: "",

};

function Chat(props) {
  const openChat = props;
  const [form, setform] = useState(initialForm);
  const { meet } = useContext(AuthStateContext);
  const [selectImagen, setSelectImagen] = useState(0);
  const [data, setdata] = useState([]);
  const [animatecloset, setanimatecloset] = useState(false);
  const [emojiOpen, setEmojiOpen] = useState(false);
  const [typefile, setTypefile] = useState(false);
  const [visorfile, setVisorfile] = useState(false);
  const [prewimg, setPrewimg] = useState([]);
  const [files, setFiles] = useState([]);

  const fileRefs = {
    file: useRef(null),
  };
  const { fetch: fetchData, isLoading: isLoadingData } = useFetch({
    initialUri: "/api/message/list",
    // contentType: "multipart/form-data",
  });
  const { submit, isLoading } = useSubmitForm({
    initialUri: "/api/message/send",
    contentType: "multipart/form-data",
  });

  const [info, setInfo] = useState({
    user_code: meet.user,
    room_chat: "V002",
    id_chat: 1,
  });

  const LoadDataChat = async () => {
    const response = await fetchData({
      id_chat: 1,
      page: 1,
      quantity_record: 10
    });
    if (response.success) {
      setdata(response.data);
    }
  };

  useEffect(() => {
    if (data.length === 0) {
      LoadDataChat();
    }
  }, []);

  const addNewMessage = (response) => {
    const message = response.id_web_message
    const busqueda = data.find(ev => ev.id_web_message == message)
    if (busqueda) {
      const resultado = data.map(ev => {
        if (ev.id_web_message == message) {
          return response
        } else {
          return ev
        }
      });
      setdata(resultado)
    } else {
      setdata(prev => [response, ...prev])
    }
  }

  useEffect(() => {
    const ENDPOINT = "https://development-virtualface-chat.ws.solera.pe";
    const socket = socketIOClient.connect(ENDPOINT);
    socket.on("V002", (response) => {
      addNewMessage(response)
    });

    return () => socket.disconnect();
  }, [data]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (form.text || files.length) {
      const arrayOfData = [...data];
      const waitmessage = {
        creation_datetime: "",
        enabled: 1,
        id_chat: "",
        text: form.text,
        user_code: meet.user,
        wait: true,
        id_web_message: form.id_web_message,
        file_type: "",
        file_path: files.length ? files[0] : "",
      };
      setVisorfile(false);
      arrayOfData.unshift(waitmessage);
      setdata(arrayOfData);
      setform({
        ...form,
        text: "",
        file_path: "",
        id_web_message: "",
      });
      setFiles([]);
       await submit(form);
    }
  }

  const handlesetTypefile = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setTypefile(!typefile);
  };
  const handleVisorFilesOpen = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (!visorfile) {
      setVisorfile(true);

    }
  };
  const handleVisorFilesCloset = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (visorfile) {
      setVisorfile(false);
      setFiles([]);
      setanimatecloset(true);
    }
  };

  const handleDropFile = (event) => {
    event.preventDefault();
    event.stopPropagation();
    let dataTransfer = event.dataTransfer;
    let files = dataTransfer.files;

    const arrayOfFiles = [];
    for (let index = 0; index < files.length; index++) {
      arrayOfFiles.push(files[index]);
      previewFile(files[index]);
    }
    setFiles(arrayOfFiles);
    setform({
      ...form,
      file_path: files[0],
    });
  };

  const previewFile = (file) => {
    if (window.FileReader) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function () {
        setPrewimg((prevState) => [...prevState, reader.result]);
      };
    }
  };

  const handleEmoji = () => {
    setEmojiOpen(!emojiOpen);
  };

  const handleEmojiSelect = (emoji) => {
    const myEmoji = emoji.native;
    setform({
      ...form,
      text: form.text + myEmoji,
    });
    setEmojiOpen(!emojiOpen);
  };


  const handleChange = (e) => {
    const { name, value } = e.target;
    const { user_code, room_chat, id_chat } = info;
    const LocalID = new Date().getTime().toString()
    setform({
      ...form,
      text: value,
      user_code,
      id_chat,
      room_chat,
      file_path: files[0],
      id_web_message: LocalID,
    });
  };
  const handleFileClick = () => {
    fileRefs[ref].current.click();
  };
  const handleFileChange = () => {
    const file = fileRefs[ref].current.files;
    if (file) {
      const arrayOfFiles = [];
      for (let index = 0; index < file.length; index++) {
        arrayOfFiles.push(file[index]);
        setFiles(arrayOfFiles)
      }
      setform({
        ...form,
        file_path: files[0],
      });
      setVisorfile(true)
    }
  };
  return (
    <Form onSubmit={handleSubmit}>
      <Exit onClick={openChat.openChat}>

        <Icon color="white" name="exit" width={15} height={15} />
      </Exit>
      {/* VISOR DE IMAGEN */}
      <OverlayVisor
        open={visorfile}
        hidden={files.length}
        onDrop={(event) => handleDropFile(event)}
        onDragLeave={(event) => handleVisorFilesCloset(event)}
        onDragOver={(event) => handleVisorFilesOpen(event)}
      ></OverlayVisor>
      <VisorFile open={visorfile}>
        <ExitVisor
          open={files.length}
          onClick={(event) => handleVisorFilesCloset(event)}
        >
          <Icon color="white" name="exit" width={15} height={15} />
        </ExitVisor>

        {files.length > 0 ? (
          <div>
            <ImgVisor src={imgPapel} alt="as" />
            <SendVisor>
              <div>
                <VisorFleInput
                  onKeyDown={(e) => {
                    if (e.keyCode == 13) {
                      handleSubmit(e);
                    }
                  }}
                  type="text"
                  value={form.text}
                  name="message"
                  onChange={handleChange}
                  placeholder="Escribe tu mensaje aquí"
                />
                <Line />
              </div>
              {/* <ButtonIcon
                onClick={handleEmoji}
                size={"small"}
                bg={"transparent"}
                iconName="emoji"
                tooltip={false}
                label="Emoji"
              /> */}
              <ButtonIcon
                type="submit"
                size={"normal"}
                bg={"primary"}
                iconName="send"
                tooltip={false}
                label="Enviar Mensaje"
              />
            </SendVisor>

            <ContainerImg>
              {files.map((elements, index) => (
                <ImgPrev
                  onClick={() => setSelectImagen(index)}
                  src={imgPapel}
                  alt="as"
                ></ImgPrev>
              ))}
            </ContainerImg>
          </div>
        ) : (
            "Suelta el archivo aca"
          )}
      </VisorFile>

      <ContainerMessages onDragEnter={(event) => handleVisorFilesOpen(event)}>
        {data.map((elements, index) => (
          <>
            <Message
              draggable="false"
              key={index}
              wait={elements.wait}
              received={elements.user_code == info.user_code}
              text={elements.text}
              file={elements.file_path}
              mb={
                data[index !== 0 ? index - 1 : index].user_code ==
                  elements.user_code
                  ? 1
                  : 3
              }
            />
          </>
        ))}
      </ContainerMessages>
      <ContainerSendMessage draggable="false">
        <ContainerInput
          onKeyDown={(e) => {
            if (e.keyCode == 27) {
              setEmojiOpen(false);
            }
          }}
          draggable="false"
        >
          <ContainerEmoji open={emojiOpen}>
            <Picker
              onSelect={(emoji) => {
                handleEmojiSelect(emoji);
              }}
              showPreview={false}
              showSkinTones={false}
              style={{
                width: "100%",
                // position: "absolute",
                // bottom: "60px",
              }}
            />
          </ContainerEmoji>

          {/* <ButtonIcon
            onClick={handleEmoji}
            size={"small"}
            bg={"transparent"}
            iconName="emoji"
            tooltip={false}
            label="Emoji"
          /> */}
          <ConInput>
            <Input
              onKeyDown={(e) => {
                if (e.keyCode == 13) {
                  handleSubmit(e);
                }
              }}
              type="text"
              value={form.text}
              name="message"
              onChange={handleChange}
              placeholder="Escribe tu mensaje aquí"
            />
            <ContainerButtonFile
              onKeyDown={(e) => {
                if (e.keyCode == 27) {
                  setTypefile(false);
                }
              }}
              draggable="false"
            >
              <ButtonIcon
                onClick={handlesetTypefile}
                size={"min"}
                bg={"transparent"}
                iconName="adjuntar"
                hoverEffect={false}
                tooltip={false}
                label="Adjuntar archivo"
              />

              <ContainerSelectTypeFile open={typefile}>

                <InputFile type="file" ref={fileRefs.file} name="filename"
                  multiple="multiple"
                  onChange={(e) => handleFileChange(e, 'file')}
                />
                <ImgTypeFile draggable="false" src={imgGaleria} alt="galeria" onClick={(e) => handleFileClick(e, 'file')} />
                <ImgTypeFile draggable="false" src={imgPapel} alt="galeria" onClick={(e) => handleFileClick(e, 'file')} />
              </ContainerSelectTypeFile>
            </ContainerButtonFile>
          </ConInput>
        </ContainerInput>

        <ButtonIcon
          type="submit"
          size={"normal"}
          bg={"primary"}
          iconName="send"
          tooltip={false}
          label="Enviar Mensaje"
        />
      </ContainerSendMessage>
    </Form>
    // </Wrapper>
  );
}

Chat.defaultProps = defaultProps;
export default Chat;

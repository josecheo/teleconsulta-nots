import React from "react";
import { Route } from "react-router-dom";


const defaultProps = {
  isProtected: true,
};

const RouterOutlet = (route) => {
  const render = (props) => {
    const component = (
      <route.component
        {...props}
        config={{
          name: route.name,
        }}
      />
    );
    return component;
  };

  return <Route path={route.path} render={render} />;
};

RouterOutlet.defaultProps = defaultProps;

export default RouterOutlet;

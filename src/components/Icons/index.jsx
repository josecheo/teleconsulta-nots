import PropTypes, { InferProps } from 'prop-types';
import React from 'react';
import svgs from './base';
import StyledSvg from './styles';

const propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
}


function Icons(props) {
  const { name, color} = props;
  const renderSvg = svgs[name] || svgs.default;

  return (
    <StyledSvg
      {...props}
      fill={color}
      viewBox={renderSvg.viewBox}
      xmlns="http://www.w3.org/2000/svg"
    >
      {renderSvg.svg}
    </StyledSvg>
  );
}

export default Icons;

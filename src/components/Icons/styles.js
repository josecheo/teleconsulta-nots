import styled from 'styled-components';
import { height, layout, space, width } from 'styled-system';

const StyledSvg = styled.svg`
fill: ${(props) => props.color};
/* ${space};
${width};
${height}; */
`;

export default StyledSvg;

// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
import React, { useContext, useEffect, useRef, useState } from 'react';
import getChimeContext from '../../context/getChimeContext';
import { Wrapper } from './styles';
import { DefaultModality } from 'amazon-chime-sdk-js';

function VideoRemote(props) {
  const chime = useContext(getChimeContext());
  const [enabled, setEnabled] = useState(false);

  useEffect(() => {
    const videoElementRemote = document.getElementById('videoElementRemote')
    const contentTileIds = new Set();
    chime?.audioVideo?.addObserver({
      videoTileDidUpdate: (tileState) => {
        if (
          !tileState.boundAttendeeId ||
          !tileState.isContent ||
          !tileState.tileId ||
          tileState.localTile
        ) {
          return;
        }

        const modality = new DefaultModality(tileState.boundAttendeeId);
        if (
          modality.base() ===
          chime?.meetingSession?.configuration.credentials?.attendeeId &&
          modality.hasModality(DefaultModality.MODALITY_CONTENT)
        ) {
          return;
        }

        chime?.audioVideo?.bindVideoElement(
          tileState.tileId,
          (videoElementRemote)
        );

        if (tileState.active) {
          contentTileIds.add(tileState.tileId);
          setEnabled(true);
        } else {
          contentTileIds.delete(tileState.tileId);
          setEnabled(contentTileIds.size > 0);
        }
      },
      videoTileWasRemoved: (tileId) => {
        if (contentTileIds.has(tileId)) {
          contentTileIds.delete(tileId);
          setEnabled(contentTileIds.size > 0);
        }
      }
    });
  }, [])




  return (
    <Wrapper id='videoElementRemote' muted />
  );
}
export default VideoRemote
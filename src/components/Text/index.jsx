import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { typography } from 'styled-system';

const StyledText = styled.p`
  ${typography}
`;

const propTypes = {
  tag: PropTypes.string.isRequired,
  fontSize: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.shape({}),
  ]),
  children: propTypes.node.isRequired,
};

const defaultProps = {
  fontSize: 16,
};



function Text(props) {
  const { children, tag, ...otherProps } = props;  

  return (
    <StyledText as={tag} {...otherProps}>
      {children}
    </StyledText>
  )
}

export default Text;

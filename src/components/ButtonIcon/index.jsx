import PropTypes from "prop-types";
import React from "react";
import Icon from "../Icons";
import Button from "./styles";

// const propTypes = {
//   iconName: PropTypes.string.isRequired,
//   label: PropTypes.string.isRequired,
//   tooltip: PropTypes.bool,
//   size: PropTypes.string,
//   bg: PropTypes.string,
//   mr: PropTypes.number,
//   hoverEffect: PropTypes.bool.isRequired,
//   type: PropTypes.string.isRequired,
// };

const defaultProps = {
  tooltip: true,
  size: "normal",
  bg: "gray",
  mr: 0,
  hoverEffect: true,
  type: "button",
};

function ButtonIcon(props) {
  const {
    iconName,
    label,
    tooltip,
    type,
    mr,
    hoverEffect,
    ...otherProps
  } = props;

  return (
    <Button
      mr={mr}
      hoverEffect={hoverEffect}
      type={type}
      // type="button"
      {...otherProps}
    >
      <Icon name={iconName} color="#FFF" />
    </Button>
  );
}

ButtonIcon.defaultProps = defaultProps;

export default ButtonIcon;

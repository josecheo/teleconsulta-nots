import styled, { css } from "styled-components";
import { variant, color, space, margin } from "styled-system";

const buttonSize = variant({
  prop: "size",
  variants: {
    min: {
      width: "1rem",
      height: "1rem",
    },
    small: {
      width: "1.7rem",
      height: "1.7rem",
    },
    normal: {
      width: "3rem",
      height: "3rem",
    },
    large: {
      width: "4rem",
      height: "4rem",
    },
  },
});

const Button = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  ${buttonSize};
  ${color};
  ${space}
  svg {
    width: 20px;
    height: 20px;
  }
  transition: box-shadow 0.5s linear 0.03s;
  ${(props) =>
    props.hoverEffect &&
    css`
      &:hover {
        box-shadow: 3px 3px 7px -1px rgba(0, 0, 0, 0.85);
      }
    `};
`;

export default Button;

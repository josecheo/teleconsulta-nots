import React, { useContext, useEffect, useRef, useState } from 'react';
import getChimeContext from '../../context/getChimeContext';
import { Wrapper } from './styles';

function VideoLocal() {
  const [enabled, setEnabled] = useState(false);
  const chime = useContext(getChimeContext());
  const videoElement = useRef(null);

  useEffect(() => {
    chime?.audioVideo?.addObserver({
      videoTileDidUpdate: (tileState) => {
        if (
          !tileState.boundAttendeeId ||
          !tileState.localTile ||
          !tileState.tileId ||
          !videoElement.current
        ) {
          return;
        }
        chime?.audioVideo?.bindVideoElement(
          tileState.tileId,
          (videoElement.current)
        );
        setEnabled(tileState.active);
      }
    });
  });


  return (
    <Wrapper ref={videoElement} id='videoElementLocal' muted />

  );
}
export default VideoLocal
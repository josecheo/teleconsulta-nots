import styled from 'styled-components';
import { height, layout, space, width } from 'styled-system';

export const Wrapper = styled.video`
display:flex;
width: 100%;
background:${(props) => props.theme.colors.secondary};
border-radius: 24px;
`;

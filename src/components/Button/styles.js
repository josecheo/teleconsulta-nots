import styled, { css } from "styled-components";
import { space, color, layout, background } from "styled-system";

export const StyledButton = styled.button`
  display: flex;
  justify-content:space-evenly;
  font-family: ${(props) => props.theme.fonts.primary};
  cursor: pointer;
  padding: .75rem 3rem;
  border-radius: 37.5px;
  width: 100%;
  ${color}
  ${space}
  ${layout}
  ${background}
`;

export const StyledIcon = styled.div`
  display: flex;
  justify-content:center;
  align-items:center;
`;

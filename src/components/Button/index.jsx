import PropTypes, { InferProps } from "prop-types";
import React from "react";
import { StyledButton, StyledIcon } from "./styles";

const propTypes = {
  renderIcon: PropTypes.node.isRequired,
  label: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  bg: PropTypes.string.isRequired,
};

const defaultProps = {
  renderIcon: null,
  label: "",
  bg: "gray",
  color: "white",
};

function Button(props) {
  const { label, renderIcon, color, bg, ...otherProps } = props;
  return (
    <StyledButton color={color} bg={bg} {...otherProps}>
      {renderIcon && <StyledIcon> {renderIcon} </StyledIcon>}
      {label}
    </StyledButton>
  );
}

Button.defaultProps = defaultProps;
export default Button;

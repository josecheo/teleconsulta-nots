import styled, { css } from "styled-components";
import { variant, color, background, space, margin } from "styled-system";

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
`;

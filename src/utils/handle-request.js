import axios from 'axios';
import handleResponse from './handle-response';

export const handleError = async (error) => {
  if (error.message === 'component unmount') return null;
  const errors = error.response || error.response;
  return errors;
};

const handleRequest = async (method, url, data, config) => {
  // try {
  const methods = {
    get: {
      ...config, method, url, params: { ...data },
    },
    post: {
      ...config, method, url, data,
    },
    put: {
      ...config, method, url, data,
    },
    delete: {
      ...config, method, url,
    },
    default: {
      ...config, method, url,
    },
  };
  const requestConfig = methods[method] || methods.default;
  const response = await axios(requestConfig)
    .then((data) => handleResponse(data))
    .catch((error) =>
      // if (!!error.request && error.request.status === 401)
      //  return {redirect: '/'};
      handleError(error));
  // if (!!response.redirect && typeof document !== 'undefined')
  //   document.location = response.redirect;
  // 01007
  return response;
};

export default handleRequest;

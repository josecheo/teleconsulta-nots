const handleSuccess = async (response) => {
  const data = Array.isArray(response.body.data)
    ? [...response.body.data]
    : typeof response.body.data === 'number'
      ? response.body.data
      : { ...response.body.data };
  const message = response.body.successMessage;
  return { success: true, data, message };
};

const handleError = async (response) => {
  const data = { ...response.data.error };
  const { errorMessage } = response.data.error;
  return { success: false, data, message: errorMessage };
};

const handleResponse = async (response) => {
  const { data } = response;
  const { success } = data;
  if (typeof success !== 'undefined') {
    if (success) {
      return handleSuccess(data);
    }
    return handleError(response);
  }
  return { success: true, data: response.data, message: '' };
};

export default handleResponse;

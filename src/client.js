import React from "react";
import { hydrate } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import ChimeProvider from './providers/ChimeProvider';
import MeetingStatusProvider from './providers/MeetingStatusProvider';
import { AuthProvider } from './providers/AuthProvider'
import { MessageProvider } from './providers/message/provider'
import App from "./App";

const Main = () => (
  <MeetingStatusProvider>
    <App />
  </MeetingStatusProvider>
);

hydrate(
  <BrowserRouter>
    <MessageProvider>
      <AuthProvider>
        <ChimeProvider>
          <Main />
        </ChimeProvider>
      </AuthProvider>
    </MessageProvider>
  </BrowserRouter>,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept();
}

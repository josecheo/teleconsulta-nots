// import Login from "./modules/Auth";
import Home from "./modules/Home";
import Call from "./modules/Call";
import Chats from "./modules/Chats";
import Login from './modules/Auth'

const routes = [...Home, ...Call, ...Chats, ...Login];

export default routes;

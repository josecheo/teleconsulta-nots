export const authReducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      return { isLoggedIn: true, meet: action.meet };
    case "LOGOUT":
      return { isLoggedIn: false, meet: {} };
    default:
      throw new Error();
  }
};

const colors = {
  primary: "#000000",
  secondary: "#E6E6E6",
  danger: "#FF0000",
  white: "#ffffff",
  black: "#000000",
  gray: "rgba(65, 65, 65, 0.5)",
};

const space = [0, 4, 8, 16, 32, 64, 128, 256, 512];

const breakpoints = ["30em", "40em", "52em", "64em", "80em"];
breakpoints.xs = breakpoints[0]; // 480px
breakpoints.sm = breakpoints[1]; // 640px
breakpoints.md = breakpoints[2]; // 832px
breakpoints.lg = breakpoints[3]; // 1024px
breakpoints.xl = breakpoints[4]; // 1280px

const fonts = {
  primary: "Gotham-normal, sans-serif",
  secondary: "Gotham-Book, sans-serif",
  light: "Gotham-Light, sans-serif",
};

export default {
  colors,
  space,
  breakpoints,
  fonts,
};

import { css } from 'styled-components';

import '../../../public/fonts/Gotham-Medium.ttf';
import '../../../public/fonts/Gotham-Medium.woff2';
import '../../../public/fonts/Gotham-Medium.woff';
import '../../../public/fonts/Gotham-Medium.eot';

import '../../../public/fonts/Gotham-Light.ttf';
import '../../../public/fonts/Gotham-Light.woff2';
import '../../../public/fonts/Gotham-Light.woff';
import '../../../public/fonts/Gotham-Light.eot';

import '../../../public/fonts/Gotham-Book.ttf';
import '../../../public/fonts/Gotham-Book.woff2';
import '../../../public/fonts/Gotham-Book.woff';
import '../../../public/fonts/Gotham-Book.eot';

const fonts = css`
  @font-face {
    font-family: "Gotham-Medium";
    src: url("/fonts/Gotham-Medium.eot");
    src: url("/fonts/Gotham-Medium.eot?#iefix")
        format("embedded-opentype"),
      url("/fonts/Gotham-Medium.woff2") format("woff2"),
      url("/fonts/Gotham-Medium.woff") format("woff"),
      url("/fonts/Gotham-Medium.ttf") format("truetype");
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: "Gotham-Book";
    src: url("/fonts/Gotham-Book.eot");
    src: url("/fonts/Gotham-Book.eot?#iefix")
        format("embedded-opentype"),
      url("/fonts/Gotham-Book.woff2") format("woff2"),
      url("/fonts/Gotham-Book.woff") format("woff"),
      url("/fonts/Gotham-Book.ttf") format("truetype");
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: "Gotham-Light";
    src: url("/fonts/Gotham-Light.eot");
    src: url("/fonts/Gotham-Light.eot?#iefix")
        format("embedded-opentype"),
      url("/fonts/Gotham-Light.woff2") format("woff2"),
      url("/fonts/Gotham-Light.woff") format("woff"),
      url("/fonts/Gotham-Light.ttf") format("truetype");
    font-weight: normal;
    font-style: normal;
  }
`;

export default fonts;
